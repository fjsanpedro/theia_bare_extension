# bare_extension
The example of how to build the Theia-based applications with the bare_extension.

## Getting started

Install [nvm](https://github.com/creationix/nvm#install-script).

    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash

Install npm and node.

    nvm install 8
    nvm use 8

Install yarn.

    npm install -g yarn

## Running the browser example with Mocked data
Inside a Rails console:
  - Create and OAuth application:

  ```
  application = FactoryBot.create(:oauth_application, scopes: 'api', owner: User.first)
  ```

  - Create an OAuth token and copy the `token` column value:

  ```
  FactoryBot.create(:oauth_access_token, resource_owner: User.first, application: application)
  ```

Update the environment variables in `browser-app/start_with_mock_data` with some mocked data:

    # Update the following values with the proper ones
    export CI_REPOSITORY_URL="http://gitlab-ci-token:abcde-1234ABCD5678ef@172.16.2.2:3001/testgroup/railsrunner.git"
    export CI_PROJECT_ID="174"
    export THEIA_MOCK_AUTH_HEADER="86b7e906ff088fc88f880f0ee4fff74e6f3ac2a784393df3b7d379aae7b21798"

Clone the repo set in `CI_REPOSITORY_URL`

```
  cd bare_extension
  yarn
  cd ..

  cd browser-app
  yarn

  ./start_with_mock_data <path_to_cloned_repo>
```

Open http://localhost:3002 in the browser.

## Developing with the browser example

Start watching of bare_extension.

    cd bare_extension
    yarn watch

Start watching of the browser example.

    cd browser-app
    yarn watch

Launch `Start Browser Backend` configuration from VS code.

Open http://localhost:3002 in the browser.

