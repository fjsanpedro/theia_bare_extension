import { injectable, inject } from 'inversify';
import { BackendApplicationContribution } from '@theia/core/lib/node';
import { Application } from 'express';
import { ILogger } from '@theia/core/lib/common/logger';
import { EnvVariable } from '@theia/core/lib/common/env-variables';
import { EnvVariablesServerImpl } from '@theia/core/lib/node/env-variables/env-variables-server';
import { Md5 } from 'ts-md5';
import * as GitLabConstants from "../common/gitlab-constants";
import { PluginsKeyValueStorage} from "@theia/plugin-ext/lib/main/node/plugins-key-value-storage"
import URI from '@theia/core/lib/common/uri';

@injectable()
export class HeadersMiddleware implements BackendApplicationContribution {
  @inject(ILogger) protected logger: ILogger
  @inject(PluginsKeyValueStorage) protected pluginManager: PluginsKeyValueStorage

  configure(app: Application): void {
    const contribution = this;

    app.use(function (req, res, next) {
      contribution.parseAuthorizationHeader(req.headers.authorization)

      next();
    });
  }

  parseAuthorizationHeader(authHeader: string | undefined): void {
    if (process.env["THEIA_MOCK_AUTH_HEADER"] != null) {
      authHeader = process.env["THEIA_MOCK_AUTH_HEADER"]
    }

    if (authHeader == null || process.env[GitLabConstants.OAUTH_TOKEN_KEY] == authHeader) {
      return
    }

    process.env[GitLabConstants.OAUTH_TOKEN_KEY] = authHeader
    this.configureGitlabPlugin()
  }

  async configureGitlabPlugin() {
    const instanceUrl = process.env[GitLabConstants.CI_API_V4_URL_KEY]
    if (instanceUrl == null) {
      throw new Error(`Gitlab Configuration failed: GitLab API url is not set`)
    }

    const url = new URI(instanceUrl)
    const validInstanceURL = `${url.scheme}://${url.authority}`

    // var currentValue = await this.pluginManager.get("fatihacet.gitlab-workflow", true)
    // if (currentValue == null) {
    //   throw new Error(`Gitlab Configuration failed: Gitlab plugin configuration failed`)
    // }

    // currentValue["glTokens"][validInstanceURL] = process.env[GitLabConstants.OAUTH_TOKEN_KEY]
    // const result = await this.pluginManager.set("fatihacet.gitlab-workflow", currentValue, true)

    // Forcing this value, since in the docker image we don't get the same results
    // we have in local
    var forcedValue:{[key:string]: any} = { }
    forcedValue["glTokens"] = {}
    forcedValue["glTokens"][validInstanceURL] = process.env[GitLabConstants.OAUTH_TOKEN_KEY]
    const result = await this.pluginManager.set("fatihacet.gitlab-workflow", forcedValue, true)
    if (!result) {
      throw new Error(`Gitlab Configuration failed: Gitlab plugin configuration failed`)
    }
  }
}


@injectable()
export class NewEnvServiceImpl extends EnvVariablesServerImpl {
    protected currentHash: string | Int32Array;

    constructor() {
      super()
      this.currentHash = this.generateCurrentHash()
    }

    async getVariables(): Promise<EnvVariable[]> {
      this.reload()
      return super.getVariables();
    }

    async getValue(key: string): Promise<EnvVariable | undefined> {
      this.reload()
      return super.getValue(key);
    }

    reload(): void {
      if (this.currentHash != this.generateCurrentHash()) {
        const prEnv = process.env;
        Object.keys(prEnv).forEach((key: string) => {
            this.envs[key] = {'name' : key, 'value' : prEnv[key]};
        });
      }
    }

    protected generateCurrentHash(): string | Int32Array {
      return Md5.hashStr(JSON.stringify(process.env))
    }
}
