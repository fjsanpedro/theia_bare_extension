/**
 * Generated using theia-extension-generator
 */

import { HeadersMiddleware, NewEnvServiceImpl } from './bare_extension-backend-contribution';
import { BackendApplicationContribution } from '@theia/core/lib/node';
import { ContainerModule } from 'inversify';
import { EnvVariablesServer } from '@theia/core/lib/common/env-variables';

export default new ContainerModule((bind, unbind, isBound, rebind) => {
    unbind(EnvVariablesServer)
    bind(EnvVariablesServer).to(NewEnvServiceImpl);
    bind(BackendApplicationContribution).to(HeadersMiddleware);;
});
