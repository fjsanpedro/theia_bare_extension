import { injectable, inject } from "inversify";
import { CommandContribution, CommandRegistry, MenuContribution, MenuModelRegistry, MessageService, CommandService } from "@theia/core/lib/common";
// import { FrontendApplicationContribution, FrontendApplication } from "@theia/core/lib/browser";
import { CommonMenus, FrontendApplicationContribution, FrontendApplication, StatusBar, QuickOpenService, QuickOpenItem, QuickOpenOptions, QuickOpenMode } from "@theia/core/lib/browser";
import { FileNavigatorContribution } from '@theia/navigator/lib/browser/navigator-contribution';
// import { GitContribution } from '@theia/git/lib/browser/git-contribution';
// import { BackendApplicationContribution } from '@theia/core/lib/node';
// import * as express from 'express';

import { GitRepositoryTracker } from '@theia/git/lib/browser/git-repository-tracker';
import { WorkspaceService } from '@theia/workspace/lib/browser';
import { GitRepositoryProvider } from '@theia/git/lib/browser/git-repository-provider';
import { Git } from '@theia/git/lib/common/git'
import { EnvVariable } from "@theia/core/lib/common/env-variables";
// import { GitSyncService } from '@theia/git/lib/browser/git-sync-service'
import { StorageService } from '@theia/core/lib/browser';
import { GitLabModel, GitLabUser, GitLabGroup, GitLabProject } from "../common/gitlab-model";
import { ThemeService } from '@theia/core/lib/browser/theming';
import { Widget } from '@phosphor/widgets';
import { GitContribution } from "@theia/git/lib/browser/git-contribution";
import { GitLabCommand } from "../common/gitlab-commands";

const icons = require('../../src/browser/style/variables.useable.css');

export const GitlabExtensionCommand = {
  id: 'GitlabExtension.command',
  label: "Shows a message"
};

@injectable()
export class GitlabCommandContribution implements CommandContribution {

  @inject(QuickOpenService) protected readonly quickOpenService: QuickOpenService
  @inject(GitLabModel) protected readonly gitLabModel: GitLabModel

  registerCommands(registry: CommandRegistry): void {
    registry.registerCommand(GitLabCommand.fork, {
      execute: () => this.showCreateForkMenu()
    });

    registry.registerCommand(GitLabCommand.showForks, {
      execute: () => this.showForkMenu()
    });
  }

  async showCreateForkMenu(): Promise<void> {
    const items: QuickOpenItem[] = [];
    const groups = await this.gitLabModel.getUserGroupsWithDeveloperAccess()
    const projects = await this.gitLabModel.forksForProject()

    groups.forEach((group:GitLabGroup) => {
      const forkFound = projects.find(project => project.namespaceFullPath == group.fullPath)

      if (forkFound) {
        return
      }

      const label = group.name
      const description = group.fullPath

      const quickItem = new QuickOpenItem({
        label,
        description,
        run: mode => {
          if (mode !== QuickOpenMode.OPEN) {
            return false;
          }

          this.gitLabModel.createFork(group.id)
          return true
        }
      })

      items.push(quickItem)
    })

    const options = QuickOpenOptions.resolve({
      placeholder: "Select the namespace to create the fork",
      fuzzyMatchLabel: true,
      fuzzySort: false,
      onClose: () => { }
    })

    this.quickOpenService.open({
      onType(lookFor: string, acceptor: (items: QuickOpenItem[]) => void): void {
          acceptor(items);
      }
    }, options);
  }

  async showForkMenu(): Promise<void> {
    const items: QuickOpenItem[] = [];
    const forks:GitLabProject [] = await this.gitLabModel.forksForProject();

    forks.forEach((fork:GitLabProject) => {
      const label = fork.pathWithNamespace

      const quickItem = new QuickOpenItem({
        label,
        run: mode => {
          if (mode !== QuickOpenMode.OPEN) {
            return false;
          }

          this.gitLabModel.switchToFork(fork.id, fork.pathWithNamespace, fork.httpToRepo)
          return true
        }
      })

      items.push(quickItem)
    })

    const options = QuickOpenOptions.resolve({
      placeholder: "Select the fork to switch to",
      fuzzyMatchLabel: true,
      fuzzySort: false,
      onClose: () => { }
    })

    this.quickOpenService.open({
      onType(lookFor: string, acceptor: (items: QuickOpenItem[]) => void): void {
          acceptor(items);
      }
    }, options);
  }
}

@injectable()
export class GitlabExtensionMenuContribution implements MenuContribution {

  registerMenus(menus: MenuModelRegistry): void {
    // Remove complete menu nodes
    var fileMenu = menus.getMenu(CommonMenus.FILE);
    // fileMenu.removeNode('1_new');
    fileMenu.removeNode('2_open');

    // Remove single elements
    // menus.unregisterMenuAction("file.newFile", CommonMenus.FILE_NEW);
    // menus.unregisterMenuAction("file.newFolder", CommonMenus.FILE_NEW);
    menus.registerMenuAction(CommonMenus.EDIT_FIND, {
      commandId: GitlabExtensionCommand.id,
      label: 'Say Hello'
    });
  }
}

export const EnvService = Symbol('EnvService');
export interface EnvService {
  get(): Promise<EnvVariable[]>;
}

@injectable()
export class GitLabConfiguratorContribution implements FrontendApplicationContribution {

  @inject(FileNavigatorContribution)
  protected readonly navigatorContribution: FileNavigatorContribution;

  @inject(GitRepositoryTracker) protected readonly repositoryTracker: GitRepositoryTracker;
  @inject(WorkspaceService) protected readonly workspaceService: WorkspaceService;
  @inject(GitRepositoryProvider) protected readonly repositoryProvider: GitRepositoryProvider;
  @inject(Git) protected readonly gitService: Git;
  @inject(EnvService) protected readonly envService: EnvService;
  @inject(StorageService) protected readonly storageService: StorageService;
  @inject(GitLabModel) protected readonly gitLabModel: GitLabModel
  @inject(StatusBar) protected readonly statusBar: StatusBar;
  @inject(GitContribution) protected readonly gitContrib: GitContribution;

  protected gitlabStatusWidget: Widget

  async configure(app: FrontendApplication): Promise<void> {
    ThemeService.get().onThemeChange(() => this.updateIcons());
  }

  async configureGitlabEnv(app: FrontendApplication): Promise<void> {
    const instance = this

    this.envService.get().then(vars => {
      this.gitLabModel.init(vars)
    })

    this.gitLabModel.onUserLogged((event) => {
      instance.setGitLabStatus(event.user)
    })

    this.gitLabModel.onBranchChanged((event) => {
      this.statusBar.setBackgroundColor(event.branchProtected ? "red" : undefined)
    })

    this.createGitLabStatusWidget(app)
  }

  async onStart(app: FrontendApplication): Promise<void> {
    this.updateIcons()
    this.configureGitlabEnv(app)
  }

  createGitLabStatusWidget(app: FrontendApplication): void {
    this.gitlabStatusWidget = new Widget();
    this.gitlabStatusWidget.id = 'theia:gitlab-status';
    this.gitlabStatusWidget.addClass('theia-gitlab-status');
    this.gitlabStatusWidget.hide()

    // Creating img avatar element
    const test = document.createElement('img');
    test.setAttribute('src', "http://www.gravatar.com/avatar")
    test.id = "avatar"

    this.gitlabStatusWidget.node.appendChild(test)

    app.shell.addWidget(this.gitlabStatusWidget, { area: 'top' });
  }

  setGitLabStatus(user: GitLabUser) {
    const avatarElement = this.gitlabStatusWidget.node.firstElementChild
    if (avatarElement == null) {
      return
    }

    avatarElement.setAttribute('src', user.avatar_url)
    avatarElement.setAttribute('title', `${user.name}(${user.username})`)
    this.gitlabStatusWidget.show();
  }

  updateIcons(): void {
    icons.use()
  }
}

@injectable()
export class GitlabGitContribution extends GitContribution {

  @inject(CommandService) protected readonly commandService: CommandService;
  @inject(MessageService) private readonly messageService: MessageService
  @inject(GitLabModel) protected readonly gitLabModel: GitLabModel

  async commit(options: Git.Options.Commit & { message?: string } = {}): Promise<void> {
    var canPush:boolean = await this.gitLabModel.canPush()

    if (canPush) {
      return super.commit(options)
    }

    const newForkAction = "New Fork";
    const selectExistingAction = "Select existing fork"
    const selection = await this.messageService.error(`You don't have permissions to push to the repository`, newForkAction, selectExistingAction);

    if (selection == null) {
      return
    }

    var command = GitLabCommand.showForks.id

    if (selection == newForkAction) {
      command = GitLabCommand.fork.id
    }

    this.commandService.executeCommand(command);

    return
  }
}
