/**
 * Generated using theia-extension-generator
 */

import { GitlabCommandContribution, GitlabExtensionMenuContribution, GitLabConfiguratorContribution, EnvService, GitlabGitContribution } from './bare_extension-frontend-contribution';
import {
    CommandContribution,
    MenuContribution
} from "@theia/core/lib/common";
import { GitContribution } from "@theia/git/lib/browser/git-contribution";

import { FrontendApplicationContribution } from "@theia/core/lib/browser";
import { EnvVariablesServer } from '@theia/core/lib/common/env-variables';
import { ContainerModule } from "inversify";
import { GitLabModel } from '../common/gitlab-model';
import '../../src/browser/style/index.css';


export default new ContainerModule((bind, unbind, isBound, rebind) => {
  bind(EnvService).toDynamicValue(ctx => {
    // let's reuse a simple and cheap service from this package
    const envServer: EnvVariablesServer = ctx.container.get(EnvVariablesServer);
    return {
        get() {
            return envServer.getVariables();
        }
    };
  });

  bind(CommandContribution).to(GitlabCommandContribution);
  bind(MenuContribution).to(GitlabExtensionMenuContribution);

  // bind(TestGit).toSelf().inSingletonScope();
  // bind(GitContribution).to(TestGit)
  bind(FrontendApplicationContribution).to(GitLabConfiguratorContribution);
  bind(GitLabModel).toSelf().inSingletonScope();

  rebind(GitContribution).to(GitlabGitContribution);
});
