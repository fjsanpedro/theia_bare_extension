import { Command } from "@theia/core";

export namespace GitLabCommand {
  export const showForks: Command = {
    id: 'gitlab.showforks',
    label: 'GitLab: Switch To Fork'
  }

  export const fork: Command = {
    id: 'gitlab.fork',
    label: 'GitLab: Create Fork'
  }
}
