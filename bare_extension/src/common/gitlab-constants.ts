export const OAUTH_TOKEN_KEY = "OAUTH_TOKEN"
export const CI_REPOSITORY_URL_KEY = "CI_REPOSITORY_URL"
export const CI_API_V4_URL_KEY = "CI_API_V4_URL"
export const GIT_OAUTH_USER = "oauth2"
export const CI_PROJECT_ID_KEY = "CI_PROJECT_ID"
export const DEVELOPER_ACCESS = 30
