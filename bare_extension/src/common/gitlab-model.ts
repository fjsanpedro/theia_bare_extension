import { injectable, inject } from "inversify";
import { EnvVariable } from "@theia/core/lib/common/env-variables";
import { StorageService, StatusBar, StatusBarAlignment } from '@theia/core/lib/browser';
import * as GitLabConstants from './gitlab-constants'
import { MessageService, Emitter, Event } from "@theia/core";
import { Git } from '@theia/git/lib/common/git'
import { GitRepositoryProvider } from '@theia/git/lib/browser/git-repository-provider';
import URI from "@theia/core/lib/common/uri";
import { Repository, WorkingDirectoryStatus } from "@theia/git/lib/common";
import { Gitlab } from 'gitlab'
import { GitRepositoryTracker } from '@theia/git/lib/browser/git-repository-tracker';

export class GitLabUser {
  readonly id: string
  readonly username: string
  readonly name: string
  readonly email: string
  readonly avatar_url: string

  constructor(id: string, username: string, name: string, email: string, avatar_url: string) {
    this.id = id
    this.username = username
    this.name = name
    this.email = email
    this.avatar_url = avatar_url
  }
}

export class GitLabGroup {
  readonly id: string
  readonly name: string
  readonly fullPath: string

  constructor(id: string, name: string, fullPath: string) {
    this.id = id
    this.name = name
    this.fullPath = fullPath
  }
}

export class GitLabProject {
  readonly id: string
  readonly name: string
  readonly path: string
  readonly pathWithNamespace: string
  readonly httpToRepo: string
  readonly webUrl: string
  readonly namespaceFullPath: string

  constructor(id: string, name: string, path: string, pathWithNamespace: string, httpToRepo: string, webUrl: string, namespaceFullPath: string) {
    this.id = id
    this.name = name
    this.path = path
    this.pathWithNamespace = pathWithNamespace
    this.httpToRepo = httpToRepo
    this.webUrl = webUrl
    this.namespaceFullPath = namespaceFullPath
  }
}

export interface UserLoggedEvent {
  readonly user: GitLabUser
}

export interface BranchChangedEvent {
  readonly branchProtected: boolean
  readonly canPush: boolean
}

@injectable()
export class GitLabModel {
  protected readonly envs: { [key: string]: string } = {};

  @inject(StorageService) protected readonly storageService: StorageService;
  @inject(MessageService) protected readonly messageService: MessageService;
  @inject(Git) protected readonly gitService: Git;
  @inject(GitRepositoryProvider) protected readonly repositoryProvider: GitRepositoryProvider;
  @inject(GitRepositoryTracker) protected readonly repositoryTracker: GitRepositoryTracker;
  @inject(StatusBar) protected readonly statusBar: StatusBar;

  private readonly userLogged = new Emitter<UserLoggedEvent>();
  readonly onUserLogged: Event<UserLoggedEvent> = this.userLogged.event;

  private readonly branchChanged = new Emitter<BranchChangedEvent>();
  readonly onBranchChanged: Event<BranchChangedEvent> = this.branchChanged.event;


  protected currentUser: GitLabUser
  protected currentBranch?: string

  async init(vars: EnvVariable[]): Promise<void> {
    vars.forEach((value) => {
      if (value.value != null) {
        this.envs[value.name] = value.value
      }
    })

    if (this.envs[GitLabConstants.OAUTH_TOKEN_KEY] == null) {
      this.messageService.error("Gitlab Authentication failed: OAuth Token is not present")
      return
    }

    const promises = []

    const progress = await this.messageService.showProgress({ text: "Configuring Environment...", options: { cancelable: false } }, () => { })

    promises.push(this.updateGitRemoteCredentials().catch((err) => {
      this.messageService.error(err.message)
    }))

    promises.push(this.disableGPGSigning().catch((err) => {
      this.messageService.error(err.message)
    }))

    promises.push(this.getUserInfo().catch((err) => {
      this.messageService.error(err.message)
    }))

    this.currentProject()

    // Set Callbacks
    this.repositoryTracker.onGitEvent((e) => {
      if (e == null) {
        return
      }

      this.updateBranchInfo(e.status)
    })

    Promise.all(promises).then(() => {
      progress.report({ message: "Done!!" })
      progress.cancel()
    })
  }

  async switchToFork(forkId: string, forkNamespace: string, forkRepoUrl: string) {
    const currentRepo = this.repositoryTracker.selectedRepository;
    if (!currentRepo) {
      throw new Error("Not inside a git repository");
    }

    try {
      const remotes = await this.gitService.remote(currentRepo);
      const hasUpstream = remotes.some(r => r == "upstream");
      const remoteUrl = (await this.gitService.exec(currentRepo, ["remote", "get-url", "origin"])).stdout.trim();

      var newRemoteUrl = this.urlWithOauthCredentials(forkRepoUrl)
      await this.gitService.exec(currentRepo, ['remote', 'set-url', 'origin', newRemoteUrl])
      this.envs[GitLabConstants.CI_REPOSITORY_URL_KEY] = newRemoteUrl
      this.envs[GitLabConstants.CI_PROJECT_ID_KEY] = forkId

      // This is to trigger the status bar refresh and update
      if (this.repositoryTracker.selectedRepositoryStatus) {
        this.currentBranch = undefined
        this.updateBranchInfo(this.repositoryTracker.selectedRepositoryStatus)
      }

      if (!hasUpstream) {
        await this.gitService.exec(currentRepo, ["remote", "add", "upstream", remoteUrl]);
      }

      this.messageService.info(`Switched successfully to fork in namespace ${forkNamespace}`)
    } catch (error) {
      throw new Error("Failed to set fork git remotes.");
    }
  }

  urlWithOauthCredentials(url: string): string {
    var uri: URI = new URI(url)

    const regex = new RegExp(`.+:.+@(.+)`)
    const matches = regex.exec(url)

    var authority = `${uri.authority}${uri.path}`

    if (matches != null) {
      authority = matches[1]
    }

    return `${uri.scheme}://${GitLabConstants.GIT_OAUTH_USER}:${this.envs[GitLabConstants.OAUTH_TOKEN_KEY]}@${authority}`
  }

  async getProject(projectId: string): Promise<GitLabProject> {
    var api: any = this.apiServer()
    var project: any = await api.Projects.show(projectId)

    return this.parseRawDataProject(project)
  }

  async getUserGroupsWithDeveloperAccess(): Promise<GitLabGroup[]> {
    var api: any = this.apiServer()
    var groups: any = await api.Groups.all({ min_access_level: GitLabConstants.DEVELOPER_ACCESS })
    var gitlabGroups: GitLabGroup[] = []

    groups.forEach((group: any) => {
      var gitlabGroup = new GitLabGroup(group.id, group.name, group.full_path)
      gitlabGroups.push(gitlabGroup)
    })

    return gitlabGroups
  }

  async createFork(namespaceId: string): Promise<GitLabProject> {
    if (this.envs[GitLabConstants.CI_PROJECT_ID_KEY] == null) {
      throw new Error(`Gitlab Fork operation error: The project is not set in the workspace`)
    }

    const progress = await this.messageService.showProgress({ text: "Creating fork project", options: { cancelable: false } }, () => { })

    var api: any = this.apiServer()
    var project: any = await api.Projects.fork(this.envs[GitLabConstants.CI_PROJECT_ID_KEY], { namespace: namespaceId })

    const fork = this.parseRawDataProject(project)

    progress.cancel();
    this.messageService.info(`The fork in namespace ${fork.pathWithNamespace} was created successfully. You will be notified when the import is completed`)

    const forkElementId = "gitlab-fork-status"
    const statusBar = this.statusBar
    statusBar.setElement(forkElementId, {
      text: `$(refresh~spin) Importing Fork ...`,
      alignment: StatusBarAlignment.RIGHT,
      priority: 100
    });

    const interval = setInterval(() => {
      this.forkStatus(fork).then(importStatus => {
        if (importStatus != "finished" && importStatus != "failed") {
          return
        }

        clearInterval(interval)

        if (importStatus == "finished") {
          this.messageService.info(`The forking process finished successfully. Fork in namespace ${fork.pathWithNamespace} is ready`)
        } else {
          this.messageService.error(`The forking process failed`)
        }

        statusBar.removeElement(forkElementId)
      })
    }, 3000)

    return fork
  }

  async forkStatus(fork: GitLabProject): Promise<string> {
    const api: any = this.apiServer()
    var project: any = await api.Projects.show(fork.id)

    return project.import_status
  }

  async currentProject(): Promise<GitLabProject> {
    if (this.envs[GitLabConstants.CI_PROJECT_ID_KEY] == null) {
      throw new Error(`Gitlab Fork operation error: The project is not set in the workspace`)
    }

    return this.getProject(this.envs[GitLabConstants.CI_PROJECT_ID_KEY])
  }

  async canPush(): Promise<boolean> {
    var api: any = this.apiServer()
    var project: any = await api.Projects.show(this.envs[GitLabConstants.CI_PROJECT_ID_KEY])

    var project_access = project.permissions.project_access
    var group_access = project.permissions.group_access
    if (project_access == null && group_access == null) {
      return false
    }

    if (project_access != null) {
      return project_access.access_level >= GitLabConstants.DEVELOPER_ACCESS
    }

    return group_access.access_level >= GitLabConstants.DEVELOPER_ACCESS
  }

  async forksForProject(): Promise<GitLabProject[]> {
    if (this.envs[GitLabConstants.CI_PROJECT_ID_KEY] == null) {
      throw new Error(`Gitlab Fork operation error: The project is not set in the workspace`)
    }

    var gitlabProjects: GitLabProject[] = []
    var api: any = this.apiServer()
    var projects: any = await api.Projects.forks(this.envs[GitLabConstants.CI_PROJECT_ID_KEY])

    projects.forEach((project: any) => {
      var gitlabProject = this.parseRawDataProject(project)
      gitlabProjects.push(gitlabProject)
    })

    return gitlabProjects
  }

  parseRawDataProject(rawData:any): GitLabProject {
    return new GitLabProject(
      rawData.id,
      rawData.name,
      rawData.path,
      rawData.path_with_namespace,
      rawData.http_url_to_repo,
      rawData.web_url,
      rawData.namespace.full_path)
  }

  async updateBranchInfo(status: WorkingDirectoryStatus) {
    var branchProtected = false
    var canPush = true

    if (status.branch == null) {
      this.branchChanged.fire({ branchProtected, canPush })
      return
    }

    if (status.branch == this.currentBranch) {
      // NO-OP because it's the same branch
      return
    }

    this.currentBranch = status.branch

    if (status.upstreamBranch != null) {
      var api: any = this.apiServer()
      var branch: any = await api.Branches.show(this.envs[GitLabConstants.CI_PROJECT_ID_KEY], status.branch)

      branchProtected = branch.protected
      canPush = branch.can_push
    }

    this.branchChanged.fire({ branchProtected, canPush })
  }

  async disableGPGSigning() {
    const repository = this.getRepository()
    if (repository == null) {
      throw new Error(`Gitlab Configuration failed: Invalid repository`)
    }

    const result = await this.gitService.exec(repository, ['config', 'commit.gpgsign', "false"])
    if (result.exitCode != 0) {
      throw new Error(result.stderr)
    }
  }

  async updateGitRemoteCredentials() {
    if (this.envs[GitLabConstants.CI_REPOSITORY_URL_KEY] == null) {
      throw new Error(`Gitlab Configuration failed: ${GitLabConstants.CI_REPOSITORY_URL_KEY}`)
    }

    const repository = this.getRepository()
    if (repository == null) {
      throw new Error(`Gitlab Configuration failed: Invalid repository`)
    }

    // For now, right now we don't handle starting from a different repository
    // We will always start, from the project set in the env variables
    // const remoteUrl = (await this.gitService.exec(repository, ["remote", "get-url", "origin"])).stdout.trim();
    const remoteUrl = this.envs[GitLabConstants.CI_REPOSITORY_URL_KEY]

    const currentUrl = (await this.gitService.exec(repository, ["remote", "get-url", "origin"])).stdout.trim();
    const urlWithCredentials = this.urlWithOauthCredentials(remoteUrl)

    // If the url with credentials is the same as the current remote url,
    // we don't need to update it
    if (currentUrl == urlWithCredentials) {
      return
    }

    const result = await this.gitService.exec(repository, ['remote', 'set-url', 'origin', urlWithCredentials])
    if (result.exitCode != 0) {
      throw new Error(result.stderr)
    }
  }

  async getUserInfo() {
    var api: any = this.apiServer()
    var user: any = await api.Users.current()

    this.currentUser = new GitLabUser(user.id, user.username, user.name, user.email, user.avatar_url)
    this.messageService.info(`Gitlab successfully authenticated with user: ${this.currentUser.username}`)

    const repository = this.getRepository()
    if (repository == null) {
      throw new Error(`Gitlab Configuration failed: Invalid repository`)
    }

    this.gitService.exec(repository, ['config', 'user.email', this.currentUser.email])
    this.gitService.exec(repository, ['config', 'user.name', this.currentUser.name])

    this.userLogged.fire({ user })
  }

  apiServer(): object {
    const apiValue = this.envs[GitLabConstants.CI_API_V4_URL_KEY]
    if (apiValue == null) {
      throw new Error(`Gitlab Configuration failed: GitLab API url is not set`)
    }

    const url = new URI(apiValue)
    const token = this.envs[GitLabConstants.OAUTH_TOKEN_KEY]
    return new Gitlab({ host: `${url.scheme}://${url.authority}`, oauthToken: token })
  }

  getRepository(): Repository | undefined {
    return this.repositoryProvider.selectedRepository
  }
}
